FactoryGirl.define do
  factory :user do
    name      "Fred Pig"
    email     "fred@pig.com"
    password  "pigsrule"
    password_confirmation  "pigsrule"
  end
end